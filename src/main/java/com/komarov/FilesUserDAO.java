package com.komarov;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FilesUserDAO extends UserDAO {

    private Client client;
    private Admin admin;
    private static final String CLIENT_FILE_PATH = "C:/Users/Антон/Desktop/clients.csv";
    private static final String LOTS_FILE_PATH = "C:/Users/Антон/Desktop/lots.csv";
    private Scanner in;

    public FilesUserDAO() {
        in = new Scanner(System.in);
    }

    public FilesUserDAO(User user) {
        if (user instanceof Client) {
            this.client = (Client) user;
        }
        if (user instanceof Admin) {
            this.admin = (Admin) user;
        }
        in = new Scanner(System.in);
    }

    @Override
    public boolean showAllClients() {
        File file = new File(CLIENT_FILE_PATH);
        Scanner x = null;
        boolean res = false;
        try {
            x = new Scanner(file);
            if (!x.hasNextLine() || file.length() <= 3) {
                System.out.println("Пока не добавлено ни одного пользователя");
                res = false;
            } else {
                int k = 1;
                System.out.println("Выберите пользователя:");
                String line;
                while (x.hasNextLine()) {
                    line = x.nextLine();
                    System.out.println(" " + k + ") " + line.split(",")[3] + " " + line.split(",")[4].charAt(0) + "." + line.split(",")[5].charAt(0) + ".");
                    k++;
                }
                res = true;
            }
            x.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (x != null) x.close();
        }
        return res;
    }

    @Override
    boolean listIsEmpty() {
        File file = new File(CLIENT_FILE_PATH);
        Scanner x = null;
        boolean res;
        try {
            x = new Scanner(file);
            if (!x.hasNextLine() || file.length() <= 3) {
                res = true;
            } else {
                res = false;
            }
            return res;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return true;
        } finally {
            if (x != null) x.close();
        }
    }

    @Override
    public Client addClient() {
        String name;
        String surname;
        String patronymic;
        String date;
        String login;
        String password;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        boolean find = false;
        if (fileSearch(CLIENT_FILE_PATH, login) >= 0) find = true;
        while (find) {
            find = false;
            System.out.print("Такой логин уже существует!\nВведите другой логин: ");
            login = in.nextLine();
            if (fileSearch(CLIENT_FILE_PATH, login) >= 0) find = true;
        }
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        System.out.println("Введите данные нового пользователя");
        System.out.print(" Фамилия: ");
        surname = in.nextLine();
        System.out.print(" Имя: ");
        name = in.nextLine();
        System.out.print(" Отчество: ");
        patronymic = in.nextLine();
        System.out.print(" Дата рождения (в формате дд.мм.гггг): ");
        date = in.nextLine();
        client = new Client(surname, name, patronymic, date, String.valueOf(System.nanoTime()));
        addToFile(CLIENT_FILE_PATH, client.getId() + "," + login + "," + password + "," + surname + "," + name + "," + patronymic + "," + client.getStringDate());
        System.out.println("\nПользователь \""+surname+" "+name+" "+patronymic+"\" добавлен!");
        return client;
    }

    private void addLot(String lotName) {
        if (fileSearch(LOTS_FILE_PATH, lotName) < 0) {
            addToFile(LOTS_FILE_PATH, lotName);
            writeMyLot();
        }
    }

    @Override
    public void showLots(User user) {
        File file = new File(LOTS_FILE_PATH);
        Scanner x = null;
        try {
            System.out.println("Список всех лотов:");
            x = new Scanner(file);
            if (!x.hasNextLine() || file.length() <= 3) {
                System.out.println("Пока не добавлено ни одного лота");
            } else {
                ArrayList<String> list = new ArrayList<>();
                String name;
                int k = 1;
                boolean find;
                while (x.hasNextLine()) {
                    find = false;
                    name = x.nextLine().split(",")[0];
                    if ((int) name.charAt(0) == 65279) name = name.substring(1);
                    for (String s : list) {
                        if (name.equals(s)) find = true;
                    }
                    if (!find) {
                        list.add(name);
                        System.out.println(" " + k + ") " + name);
                        k++;
                    }
                }
            }
            System.out.println("\nВведите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) user.showAbilities();
            x.close();
        } catch (FileNotFoundException e) {
            System.out.println("Пока не добавлено ни одного лота");
            user.showAbilities();
        } finally {
            if (x != null) x.close();
        }
    }

    @Override
    public Client selectClient(int c) {
        File file = new File(CLIENT_FILE_PATH);
        Scanner x = null;
        Client cl = null;
        try {
            x = new Scanner(file);
            int k = 0;
            String line;
            while (x.hasNextLine()) {
                line = x.nextLine();
                if (k == (c - 1)) {
                    String[] str = line.split(",");
                    cl = new Client(str[3], str[4], str[5], str[6], str[0]);
                    break;
                }
                k++;
            }
            x.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (x != null) x.close();
        }
        return cl;
    }

    @Override
    public void newLot() {
        System.out.println("Добавление нового лота");
        System.out.print(" Введите название лота: ");
        String lotName = in.nextLine();
        boolean find = false;
        if (fileSearch(LOTS_FILE_PATH, lotName) >= 0) find = true;
        if (!find) {
            if (client != null) {
                System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
                if (in.nextLine().equals("1")) {
                    System.out.print("Введите ставку: ");
                    addToFile(LOTS_FILE_PATH, lotName + "," + client.getId() + "," + Double.parseDouble(in.nextLine()));
                    writeMyLot();
                    System.out.println("Лот \"" + lotName + "\" добавлен!");
                    System.out.println("\nВведите \"0\", чтобы выйти");
                    if (in.nextLine().equals("0")) client.showAbilities();
                } else {
                    addToFile(LOTS_FILE_PATH, lotName + "," + client.getId());
                    writeMyLot();
                    System.out.println("Лот \"" + lotName + "\" добавлен!");
                    client.showAbilities();
                }
            } else {
                addToFile(LOTS_FILE_PATH, lotName + ",admin");
                System.out.println("Лот \"" + lotName + "\" добавлен!");
                System.out.println("\nВведите \"0\", чтобы выйти");
                if (in.nextLine().equals("0")) admin.showAbilities();
            }
        } else {
            System.out.println("\nТакой лот уже существует!");
            if (client != null) {
                System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
                if (in.nextLine().equals("1")) {
                    System.out.print("Введите ставку: ");
                    addToFile(LOTS_FILE_PATH, lotName + "," + client.getId() + "," + Double.parseDouble(in.nextLine()));
                    writeMyLot();
                    System.out.println("Ставка к лоту \"" + lotName + "\" добавлена!");
                    System.out.println("\nВведите \"0\", чтобы выйти");
                    if (in.nextLine().equals("0")) client.showAbilities();
                } else {
                    addLot(lotName + "," + client.getId());
                    client.showAbilities();
                }
            } else {
                admin.showAbilities();
            }
        }
    }

    private void writeMyLot() {
        String oldStr = returnString(CLIENT_FILE_PATH, fileSearch(CLIENT_FILE_PATH, client.getId()));
        String newStr;
        if (oldStr.split(",").length > 7) {
            newStr = ";" + (getKolvoLines(LOTS_FILE_PATH) - 1);
        } else {
            newStr = "," + (getKolvoLines(LOTS_FILE_PATH) - 1);
        }
        editFile(CLIENT_FILE_PATH, "", newStr, fileSearch(CLIENT_FILE_PATH, client.getId()), true);

    }

    @Override
    public void showMyLotsAndStavki() {
        System.out.println("Просмотр списка своих ставок по лоту:");
        String[] strArray = returnString(CLIENT_FILE_PATH, fileSearch(CLIENT_FILE_PATH, client.getId())).split(",");
        if (strArray.length > 7) {
            String[] stroki = strArray[7].split(";");
            boolean find;
            ArrayList<String> check = new ArrayList<>();
            String nameLot;
            for (String s : stroki) {
                nameLot = returnString(LOTS_FILE_PATH, Integer.parseInt(s)).split(",")[0];
                if ((int) nameLot.charAt(0) == 65279) nameLot = nameLot.substring(1);
                find = false;
                for (String ss : check) {
                    if (ss.equals(nameLot)) find = true;
                }
                if (!find) {
                    check.add(nameLot);
                }
            }
            String[] str;
            int i = 1;
            int k;
            for (String name : check) {
                System.out.println(" " + i + ") " + name);
                i++;
                k = 0;
                for (String s : stroki) {
                    str = returnString(LOTS_FILE_PATH, Integer.parseInt(s)).split(",");
                    if (str.length > 2 && str[0].equals(name)) {
                        System.out.println("    " + (k+1) + ") " + str[2]);
                        k++;
                    }
                }
                if (k == 0) System.out.println("    (нет ставок)");
                System.out.println();
            }
        } else System.out.println("У вас нет лотов!");
        System.out.println("Введите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) client.showAbilities();
    }

    @Override
    public void getAllStavki() {
        System.out.println("Просмотр списка всех ставок");
        File file = new File(LOTS_FILE_PATH);
        Scanner x = null;
        try {
            x = new Scanner(file);
            if (!x.hasNextLine() || file.length() <= 3) {
                System.out.println("Пока не добавлено ни одного лота");
                admin.showAbilities();
            } else {
                ArrayList<String> lotNames = new ArrayList<>();
                String name;
                boolean find;
                while (x.hasNextLine()) {
                    name = x.nextLine().split(",")[0];
                    if ((int) name.charAt(0) == 65279) name = name.substring(1);
                    find = false;
                    for (String s : lotNames) {
                        if (s.equals(name)) find = true;
                    }
                    if (!find) lotNames.add(name);
                }
                x.close();
                int k = 1;
                int c;
                String[] str;
                for (String s : lotNames) {
                    x = new Scanner(file);
                    System.out.println(" " + k + ") Лот \"" + s + "\":");
                    k++;
                    c = 0;
                    while (x.hasNextLine()) {
                        str = x.nextLine().split(",");
                        if (str.length > 2 && str[0].equals(s)) {
                            System.out.println("    " + (c+1) + ") " + str[2]);
                            c++;
                        }
                    }
                    if (c == 0) System.out.println("    (нет ставок)\n");
                    else System.out.println();
                    x.close();
                }
            }
            System.out.println("Введите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) admin.showAbilities();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Пока не добавлено ни одного лота");
            admin.showAbilities();
        } finally {
            if (x != null) x.close();
        }
    }

    @Override
    public void getUsersLots() {
        System.out.println("Просмотр количества лотов пользователя");
        if (showAllClients()) {
            Client cl  = selectClient(Integer.parseInt(in.nextLine()));
            File file = new File(LOTS_FILE_PATH);
            Scanner x;
            ArrayList<String> list = new ArrayList<>();
            try {
                x = new Scanner(file);
                if (x.hasNextLine() || file.length() > 3) {
                    String line;
                    boolean find;
                    while (x.hasNextLine()) {
                        line = x.nextLine();
                        find = false;
                        if (line.split(",")[1].equals(cl.getId())) {
                            for (String s : list) {
                                if (s.equals(line.split(",")[0])) find = true;
                            }
                            if (!find) {
                                list.add(line.split(",")[0]);
                            }
                        }
                    }
                }
                x.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println("Количество лотов клиента \"" + cl.getFullName() + "\": " + list.size() + " шт.");
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    public void getUsersStavki() {
        System.out.println("Просмотр количества ставок пользователя");
        if (showAllClients()) {
            Client cl  = selectClient(Integer.parseInt(in.nextLine()));
            File file = new File(LOTS_FILE_PATH);
            Scanner x;
            int k = 0;
            try {
                x = new Scanner(file);
                if (x.hasNextLine() && file.length() > 3) {
                    String line;
                    while (x.hasNextLine()) {
                        line = x.nextLine();
                        if (line.split(",").length > 2 && line.split(",")[1].equals(cl.getId())) {
                            k++;
                        }
                    }
                    System.out.println("Количество ставок клиента \"" + cl.getFullName() + "\": " + k + " шт.");
                } else {
                    System.out.println("У пользователя \"" + cl.getFullName() + "\" нет лотов!");
                }
                x.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    public void getClientList() {
        System.out.println("Вывести список пользователей, отсортированный по:\n 1) ФИО\n 2) Дате рождения");
        String k;
        try {
            k = in.nextLine();
            File file = new File(CLIENT_FILE_PATH);
            Scanner x = new Scanner(file);
            if (x.hasNextLine() && file.length() > 3) {
                do {
                    switch (k) {
                        case "1":
                            TreeMap<String, String> clientList1 = new TreeMap<>();
                            String[] line1;
                            String name;
                            while (x.hasNextLine()) {
                                line1 = x.nextLine().split(",");
                                name = line1[3] + " " + line1[4].charAt(0) + "." + line1[5].charAt(0) + ".";
                                clientList1.put(name, line1[0]);
                            }
                            int i = 1;
                            System.out.println("\nСписок пользователей:");
                            for (Map.Entry<String, String> item : clientList1.entrySet()) {
                                System.out.println(" " + i + ") " + item.getKey());
                                i++;
                            }
                            break;
                        case "2":
                            TreeMap<Date, String> clientList2 = new TreeMap<>();
                            String[] line2;
                            String name2;
                            Date date;
                            while (x.hasNextLine()) {
                                line2 = x.nextLine().split(",");
                                name2 = line2[3] + " " + line2[4].charAt(0) + "." + line2[5].charAt(0) + ".";
                                try {
                                    date = new SimpleDateFormat("d.MM.yyyy").parse(line2[6]);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    date = new Date();
                                }
                                clientList2.put(date, name2);
                            }
                            int j = 1;
                            System.out.println("\nСписок пользователей:");
                            for (Map.Entry<Date, String> item : clientList2.entrySet()) {
                                System.out.println(" " + j + ") " + item.getValue());
                                j++;
                            }
                            break;
                        default:
                            System.out.println("Введите число 1 или 2!");
                            break;
                    }
                } while (!k.equals("1") && !k.equals("2"));
            } else {
                System.out.println("Пока не добавлено ни одного пользователя");
            }
            x.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    Client clientLogIn() {
        String login;
        String password;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        int k = fileSearch(CLIENT_FILE_PATH, login + "," + password);
        if (k < 0) {
            System.out.println("Неверный логин или пароль!\n");
            return null;
        } else {
            String[] clientInfo = returnString(CLIENT_FILE_PATH, k).split(",");
            Client cc = new Client(clientInfo[3], clientInfo[4], clientInfo[5], clientInfo[6], clientInfo[0]);
            cc.setLogin(login);
            cc.setPassword(password);
            System.out.println("Вы вошли как \"" + cc.getFullName() + "\"");
            return cc;
        }
    }

    private void addToFile(String path, String addString) {
        PrintWriter printWriter = null;
        try {
            FileWriter fileWriter = new FileWriter(path, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            printWriter.println(addString);
            printWriter.flush();
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) printWriter.close();
        }
    }

    private int getKolvoLines(String path) {
        int k = 0;
        File file = new File(path);
        Scanner x = null;
        try {
            x = new Scanner(file);
            while (x.hasNextLine()) {
                x.nextLine();
                k++;
            }
            x.close();
            return k;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return 0;
        } finally {
            if (x != null) x.close();
        }
    }

    private int fileSearch(String path, String searchString) {
        int res = -1;
        File file = new File(path);
        Scanner x = null;
        try {
            x = new Scanner(file);
            String line;
            int k = 0;
            while (x.hasNextLine()) {
                line = x.nextLine();
                if (line.contains(searchString)) {
                    res = k;
                    break;
                }
                k++;
            }
            x.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -2;
        } finally {
            if (x != null) x.close();
        }
        return res;
    }

    private String returnString(String path, int strNumber) {
        String string = "";
        File file = new File(path);
        Scanner x = null;
        try {
            x = new Scanner(file);
            int k = 0;
            String line;
            while (x.hasNextLine()) {
                line = x.nextLine();
                if (k == strNumber) {
                    string += line;
                    break;
                }
                k++;
            }
            x.close();
            return string;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return string;
        } finally {
            if (x != null) x.close();
        }
    }

    private void editFile(String path, String oldString, String newString, int strNumber, boolean append) {
        File oldFile = new File(path);
        File newFile = new File(oldFile.getParent() + "/new" + oldFile.getName());
        Scanner x = null;
        try {
            FileWriter fileWriter = new FileWriter(newFile, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter);
            x = new Scanner(oldFile);
            String line;
            int k = 0;
            while (x.hasNextLine()) {
                line = x.nextLine();
                if (strNumber == -1) {  //заменяем во всем файле
                    if (!line.contains(oldString)) {
                        printWriter.println(line);
                    } else {
                        printWriter.println(line.replaceAll(oldString, newString));
                    }
                } else {  //заменяем в указанной строке
                    if (append) {
                        if (k == strNumber) printWriter.println(line + newString);
                        else printWriter.println(line);
                    } else {
                        if (k == strNumber && line.contains(oldString)) {
                             printWriter.println(line.replaceAll(oldString, newString));
                        } else {
                            printWriter.println(line);
                        }
                    }
                }
                k++;
            }
            x.close();
            printWriter.flush();
            printWriter.close();
            oldFile.delete();
            newFile.renameTo(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (x != null) x.close();
        }
    }
}
