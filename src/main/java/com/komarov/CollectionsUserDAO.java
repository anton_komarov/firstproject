package com.komarov;

import java.util.*;

public class CollectionsUserDAO extends UserDAO {
    private Client client;
    private Admin admin;
    private Scanner in;

    public CollectionsUserDAO(User user) {
        if (user instanceof Client) {
            this.client = (Client) user;
        }
        if (user instanceof Admin) {
            this.admin = (Admin) user;
        }
        in = new Scanner(System.in);
    }

    public CollectionsUserDAO() {
        in = new Scanner(System.in);
    }

    @Override
    public void newLot() {
        System.out.println("Добавление нового лота");
        System.out.print(" Введите название лота: ");
        addLot(in.nextLine());
        if (client != null) {
            client.getMyLots().add(Main.getLots().get(Main.getLots().size() - 1));
            System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
            if (in.nextLine().equals("1")) {
                System.out.print("Введите ставку: ");
                newStavka(client.getMyLots().size() - 1, Double.parseDouble(in.nextLine()));
                System.out.println("\nВведите \"0\", чтобы выйти");
                if (in.nextLine().equals("0")) client.showAbilities();
            } else client.showAbilities();
        } else {
            System.out.println("\nВведите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) admin.showAbilities();
        }
    }

    @Override
    public void showMyLotsAndStavki() {
        System.out.println("Просмотр списка своих ставок по лоту:");
        if (client.getMyLots().isEmpty()) {
            System.out.println("У вас нет лотов!");
        } else {
            int s;
            for (int i = 0; i < client.getMyLots().size(); i++) {
                System.out.println("\n " + (i+1) + ") Лот \"" + client.getMyLots().get(i).getName() + "\":");
                s = 0;
                for (int k = 0; k < client.getMyStavki()[i].length; k++) {
                    if (client.getMyStavki()[i][k] != 0) s++;
                }
                if (s == 0) System.out.println("    (нет ставок)");
                else {
                    for (int j = 0; j < s; j++) {
                        System.out.println("    " + (j+1) + ") " + client.getStavka(i, j));
                    }
                }
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) client.showAbilities();
    }

    private void newStavka(int myLot, Double stavka) {
        client.getMyLots().get(myLot).getStavki().add(stavka);
        for (int i = 0; i < client.getMyStavki()[myLot].length; i++) {
            if (client.getMyStavki()[myLot][i] == 0) {
                client.getMyStavki()[myLot][i] = client.getMyLots().get(myLot).getStavki().size();
                break;
            }
        }
    }

    @Override
    public void getAllStavki() {
        System.out.println("Просмотр списка всех ставок");
        if (Main.getLots().isEmpty()) {
            System.out.println("Пока не добавлено ни одного лота!");
        } else {
            Lots lot;
            for (int i = 0; i < Main.getLots().size(); i++) {
                lot = Main.getLots().get(i);
                System.out.println("\n " + (i+1) + ") Лот \"" + lot.getName() + "\":");
                if (lot.getStavki().isEmpty()) System.out.println("    (нет ставок)");
                else {
                    for (int j = 0; j < lot.getStavki().size(); j++) {
                        System.out.println("    " + (j+1) + ") " + lot.getStavki().get(j));
                    }
                }
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    public void getUsersLots() {
        System.out.println("Просмотр количества лотов пользователя");
        if (showAllClients()) {
            Client client = Main.getClients().get(Integer.parseInt(in.nextLine()) - 1);
            System.out.println("Количество лотов клиента \"" + client.getFullName() + "\": " + client.getMyLots().size() + " шт.");
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    public void getUsersStavki() {
        System.out.println("Просмотр количества ставок пользователя");
        if (showAllClients()) {
            ArrayList<Client> clientList = Main.getClients();
            Client client = clientList.get(Integer.parseInt(in.nextLine()) - 1);
            if (!client.getMyLots().isEmpty()) {
                int st = 0;
                int[][] lots = client.getMyStavki();
                for (int i = 0; i < lots.length; i++) {
                    for (int j = 0; j < lots[i].length; j++) {
                        if (lots[i][j] != 0) st++;
                    }
                }
                System.out.println("Количество ставок клиента \"" + client.getFullName() + "\": " + st + " шт.");
            } else {
                System.out.println("У пользователя \"" + client.getFullName() + "\" нет лотов!");
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    public void getClientList() {
        System.out.println("Вывести список пользователей, отсортированный по:\n 1) ФИО\n 2) Дате рождения");
        String k;
        do {
            k = in.nextLine();
            switch (k) {
                case "1":
                    TreeMap<String, Client> clientList1 = new TreeMap<>();
                    for (Client client : Main.getClients()) {
                        clientList1.put(client.getFullName(), client);
                    }
                    int i = 1;
                    System.out.println("\nСписок пользователей:");
                    for(Map.Entry<String, Client> item : clientList1.entrySet()) {
                        System.out.println(" " + i + ") " + item.getKey());
                        i++;
                    }
                    break;
                case "2":
                    TreeMap<Date, Client> clientList2 = new TreeMap<>();
                    for (Client client : Main.getClients()) {
                        clientList2.put(client.getDateOfBirth(), client);
                    }
                    int j = 1;
                    System.out.println("\nСписок пользователей:");
                    for(Map.Entry<Date, Client> item : clientList2.entrySet()) {
                        System.out.println(" " + j + ") " + item.getValue().getFullName());
                        j++;
                    }
                    break;
                default:
                    System.out.println("Введите число 1 или 2!");
                    break;
            }
        } while (!k.equals("1") && !k.equals("2"));
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    Client clientLogIn() {
        String login;
        String password;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        Client cc = null;
        for (Client c : Main.getClients()) {
            if (c.getLogin().equals(login) && c.getPassword().equals(password)) {
                cc = c;
                break;
            }
        }
        if (cc == null) {
            System.out.println("Неверный логин или пароль!\n");
        } else {
            System.out.println("Вы вошли как \"" + cc.getFullName() + "\"");
        }
        return cc;
    }

    @Override
    public boolean showAllClients() {
        if (Main.getClients().isEmpty()) {
            System.out.println("Пока не добавлено ни одного пользователя");
            return false;
        } else {
            System.out.println("Выберите пользователя:");
            for (int i = 0; i < Main.getClients().size(); i++) {
                System.out.println(" " + (i + 1) + ") " + Main.getClients().get(i).getFullName());
            }
            return true;
        }
    }

    @Override
    boolean listIsEmpty() {
        return Main.getClients().isEmpty();
    }

    @Override
    public Client addClient() {
        String name;
        String surname;
        String patronymic;
        String date;
        String login;
        String password;
        boolean find = false;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        for (Client cl : Main.getClients()) {
            if (cl.getLogin().equals(login)) find = true;
        }
        while (find) {
            find = false;
            System.out.print("Такой логин уже существует!\nВведите другой логин: ");
            login = in.nextLine();
            for (Client cl : Main.getClients()) {
                if (cl.getLogin().equals(login)) find = true;
            }
        }
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        System.out.println("\nВведите данные нового пользователя");
        System.out.print(" Фамилия: ");
        surname = in.nextLine();
        System.out.print(" Имя: ");
        name = in.nextLine();
        System.out.print(" Отчество: ");
        patronymic = in.nextLine();
        System.out.print(" Дата рождения (в формате дд.мм.гггг): ");
        date = in.nextLine();
        client = new Client(surname, name, patronymic, date, String.valueOf(System.nanoTime()));
        client.setLogin(login);
        client.setPassword(password);
        Main.getClients().add(client);
        System.out.println("\nПользователь \""+surname+" "+name+" "+patronymic+"\" добавлен!");
        return client;
    }

    private void addLot(String lotName) {
        Main.getLots().add(new Lots(lotName));
        System.out.println("Лот \"" + lotName + "\" добавлен!");
        Main.getLots().size();
    }

    @Override
    public void showLots(User user) {
        ArrayList<Lots> lots = Main.getLots();
        System.out.println("Список всех лотов:");
        if (lots.isEmpty()) {
            System.out.println("Пока не добавлено ни одного лота");
        } else {
            for (int i = 0; i < lots.size(); i++) {
                System.out.println(" " + (i + 1) + ") " + lots.get(i).getName());
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) user.showAbilities();
    }

    @Override
    public Client selectClient(int c) {
        return Main.getClients().get(c - 1);
    }
}
