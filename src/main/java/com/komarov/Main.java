package com.komarov;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static User user;
    private static ArrayList<Client> clients;
    private static ArrayList<Lots> lots;
    private static UserDAO userDAO;
    private static final int USER_DAO_CONSTANT = 4;

    public static void main(String[] args) {
        switch (USER_DAO_CONSTANT) {
            case 1:
                lots = new ArrayList<>();
                clients = new ArrayList<>();
                userDAO = new CollectionsUserDAO();
                break;
            case 2:
                userDAO = new FilesUserDAO();
                break;
            case 3:
                userDAO = new DatabaseUserDAO();
                break;
            case 4:
                userDAO = new JPAUserDAO();
                break;
        }

        login();
    }

    public static void login() {
        Scanner in = new Scanner(System.in);
        System.out.print("Войти как:\n 1) Гость\n 2) Клиент\n 3) Админ\n");
        String k;
        do {
            k = in.nextLine();
            switch (k) {
                case "1":
                    user = new Guest();
                    System.out.println("Вы вошли как Гость");
                    break;
                case "2":
                    if (!userDAO.listIsEmpty()) {
                        int c;
                        do {
                            System.out.println(" 1) Войти");
                            System.out.println(" 2) Зарегистрироваться");
                            System.out.println("\n 0) В меню");
                            c = Integer.parseInt(in.nextLine());
                            if (c == 1) {
                                user = userDAO.clientLogIn();
                            } else if (c == 0) {
                                login();
                            } else {
                                user = userDAO.addClient();
                            }
                        } while (user == null);
                    } else {
                        System.out.println("Список пользователей пуст");
                        System.out.println("Регистрация нового пользователя");
                        user = userDAO.addClient();
                    }
                    break;
                case "3":
                    if (userDAO.adminLogIn()) {
                        user = new Admin();
                        System.out.println("Вы вошли как Админ");
                    } else {
                        login();
                    }
                    break;
                default:
                    System.out.println("Введите число от 1 до 3");
            }
        } while (!k.equals("1") && !k.equals("2") && !k.equals("3") && !k.equals("0"));
        user.showAbilities();
    }

    public static ArrayList<Lots> getLots() {
        return lots;
    }

    public static ArrayList<Client> getClients() {
        return clients;
    }

    public static int getUserDaoConstant() {
        return USER_DAO_CONSTANT;
    }
}
