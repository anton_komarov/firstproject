package com.komarov;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class DatabaseUserDAO extends UserDAO {

    private Client client;
    private Admin admin;
    private Scanner in;
    private static final String driverName = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/auction";
    private static final String dbUser = "postgres";
    private static final String dbPassword = "root";

    public DatabaseUserDAO() {
        in = new Scanner(System.in);
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public DatabaseUserDAO(User user) {
        in = new Scanner(System.in);
        if (user instanceof Client) {
            this.client = (Client) user;
        }
        if (user instanceof Admin) {
            this.admin = (Admin) user;
        }
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    boolean showAllClients() {
        boolean res = false;
        try {
            Connection connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select surname, name, patronymic from Users");
            if (!resultSet.isBeforeFirst()) {
                System.out.println("Пока не добавлено ни одного пользователя");
                res = false;
            } else {
                System.out.println("Выберите пользователя:");
                int i = 1;
                while (resultSet.next()) {
                    System.out.println(" " + i + ") " + resultSet.getString(1) + " " + resultSet.getString(2).charAt(0) + "." +
                            resultSet.getString(3).charAt(0) + ".");
                    i++;
                }
                res = true;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    boolean listIsEmpty() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from Users");
            if (resultSet.next()) {
                return false;
            } else return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return true;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    Client addClient() {
        String name;
        String surname;
        String patronymic;
        String date;
        String login;
        String password;
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            System.out.print("Введите логин: ");
            login = in.nextLine();
            ResultSet resultSet = statement.executeQuery("select login from Users where login=\'" + login + "\'");
            while (resultSet.next()) {
                System.out.print("Такой логин уже существует!\nВведите другой логин: ");
                login = in.nextLine();
                resultSet = statement.executeQuery("select login from Users where login=\'" + login + "\'");
            }
            System.out.print("Введите пароль: ");
            password = in.nextLine();
            System.out.println("Введите данные нового пользователя");
            System.out.print(" Фамилия: ");
            surname = in.nextLine();
            System.out.print(" Имя: ");
            name = in.nextLine();
            System.out.print(" Отчество: ");
            patronymic = in.nextLine();
            System.out.print(" Дата рождения (в формате дд.мм.гггг): ");
            date = in.nextLine();
            client = new Client(surname, name, patronymic, date, String.valueOf(System.nanoTime()));
            client.setLogin(login);
            client.setPassword(password);
            statement.executeUpdate("insert into Users (surname, name, patronymic, date, login, password) values (\'" + surname + "\', \'" + name + "\', \'" + patronymic
                    + "\', \'" + client.getStringDate() + "\', \'" + login + "\', \'" + password + "\')");
            System.out.println("\nПользователь \"" + surname + " " + name + " " + patronymic + "\" добавлен!");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return client;
    }

    @Override
    void showLots(User user) {
        System.out.println("Список всех лотов:");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select lotName from Lots group by lotName");
            if (!resultSet.isBeforeFirst()) {
                System.out.println("Пока не добавлено ни одного лота");
            } else {
                int i = 1;
                while (resultSet.next()) {
                    System.out.println(" " + i + ") " + resultSet.getString(1));
                    i++;
                }
            }
            System.out.println("\nВведите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) user.showAbilities();
        } catch (SQLException e) {
            System.out.println("Пока не добавлено ни одного лота");
            user.showAbilities();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    Client selectClient(int c) {
        Connection connection = null;
        Client cl = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from Users");
            int i = 1;
            while (resultSet.next()) {
                if (i == c) {
                    cl = new Client(resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
                            resultSet.getString(5), resultSet.getString(1));
                    break;
                }
                i++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return cl;
    }

    @Override
    void newLot() {
        Connection connection = null;
        System.out.println("Добавление нового лота");
        System.out.print(" Введите название лота: ");
        String newLotName = in.nextLine();
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select lotName from Lots where lotName=\'" + newLotName + "\'");
            if (!resultSet.isBeforeFirst()) {
                if (client != null) {
                    System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
                    if (in.nextLine().equals("1")) {
                        System.out.print("Введите ставку: ");
                        String stavka = in.nextLine();
                        statement.executeUpdate("insert into Lots (lotName, whoAdd, stavka) values ('" + newLotName + "\', \'" + client.getId() + "\', \'"
                                + stavka + "\')");

                        System.out.println("Лот \"" + newLotName + "\" добавлен!");
                        System.out.println("\nВведите \"0\", чтобы выйти");
                        if (in.nextLine().equals("0")) client.showAbilities();
                    } else {
                        statement.executeUpdate("insert into Lots (lotName, whoAdd) values ('" + newLotName + "\', \'" + client.getId() + "\')");
                        System.out.println("Лот \"" + newLotName + "\" добавлен!");
                        client.showAbilities();
                    }
                } else {
                    statement.executeUpdate("insert into Lots (lotName, whoAdd) values ('" + newLotName + "\', \'admin\')");
                    System.out.println("Лот \"" + newLotName + "\" добавлен!");
                    admin.showAbilities();
                }
            } else {
                System.out.println("\nТакой лот уже существует!");
                if (client != null) {
                    System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
                    if (in.nextLine().equals("1")) {
                        System.out.print("Введите ставку: ");
                        String stavka = in.nextLine();
                        statement.executeUpdate("insert into Lots (lotName, whoAdd, stavka) values ('" + newLotName + "\', \'" + client.getId() + "\', \'"
                                + stavka + "\')");
                        System.out.println("Ставка к лоту \"" + newLotName + "\" добавлена!");
                        System.out.println("\nВведите \"0\", чтобы выйти");
                        if (in.nextLine().equals("0")) client.showAbilities();
                    } else {
                        statement.executeUpdate("insert into Lots (lotName, whoAdd) values ('" + newLotName + "\', \'" + client.getId() + "\')");
                        client.showAbilities();
                    }
                } else {
                    admin.showAbilities();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    void showMyLotsAndStavki() {
        System.out.println("Просмотр списка своих ставок по лоту:");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select lotName from Lots where whoAdd=\'" + client.getId() + "\' group by lotName");
            if (!resultSet.isBeforeFirst()) {
                System.out.println("У вас нет лотов!");
            } else {
                ArrayList<String> names = new ArrayList<>();
                while (resultSet.next()) {
                    names.add(resultSet.getString(1));
                }
                int k;
                for (int i = 0; i < names.size(); i++) {
                    System.out.println(" " + (i + 1) + ") " + names.get(i));
                    k = 0;
                    resultSet = statement.executeQuery("select stavka from Lots where (whoAdd=\'" + client.getId() + "\' and lotName=\'" + names.get(i) + "\')");
                    while (resultSet.next()) {
                        if (resultSet.getString(1) != null) {
                            System.out.println("    " + (k + 1) + ") " + resultSet.getString(1));
                            k++;
                        }
                    }
                    if (k == 0) System.out.println("    (нет ставок)");
                    System.out.println();
                }
            }
            System.out.println("Введите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) client.showAbilities();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    void getAllStavki() {
        System.out.println("Просмотр списка всех ставок");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select lotName from Lots group by lotName");
            if (!resultSet.isBeforeFirst()) {
                System.out.println("Пока не добавлено ни одного лота");
            } else {
                ArrayList<String> names = new ArrayList<>();
                while (resultSet.next()) {
                    names.add(resultSet.getString(1));
                }
                int k;
                for (int i = 0; i < names.size(); i++) {
                    System.out.println(" " + (i + 1) + ") " + names.get(i));
                    k = 0;
                    resultSet = statement.executeQuery("select stavka from Lots where lotName=\'" + names.get(i) + "\'");
                    while (resultSet.next()) {
                        if (resultSet.getString(1) != null) {
                            System.out.println("    " + (k + 1) + ") " + resultSet.getString(1));
                            k++;
                        }
                    }
                    if (k == 0) System.out.println("    (нет ставок)");
                    System.out.println();
                }
            }
            System.out.println("Введите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) admin.showAbilities();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    void getUsersLots() {
        System.out.println("Просмотр количества лотов пользователя");
        Connection connection = null;
        try {
            if (showAllClients()) {
                Client cl = selectClient(Integer.parseInt(in.nextLine()));
                connection = DriverManager.getConnection(URL, dbUser, dbPassword);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select lotName from Lots where whoAdd=\'" + cl.getId() + "\' group by lotName");
                int k = 0;
                while (resultSet.next()) {
                    if (resultSet.getString(1) != null) k++;
                }
                System.out.println("Количество лотов клиента \"" + cl.getFullName() + "\": " + k + " шт.");
            }
            System.out.println("\nВведите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) admin.showAbilities();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    void getUsersStavki() {
        System.out.println("Просмотр количества ставок пользователя");
        Connection connection = null;
        try {
            if (showAllClients()) {
                Client cl = selectClient(Integer.parseInt(in.nextLine()));
                connection = DriverManager.getConnection(URL, dbUser, dbPassword);
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select stavka from Lots where whoAdd=\'" + cl.getId() + "\'");
                if (!resultSet.isBeforeFirst()) {
                    System.out.println("У пользователя \"" + cl.getFullName() + "\" нет лотов!");
                } else {
                    int k = 0;
                    while (resultSet.next()) {
                        if (resultSet.getString(1) != null) k++;
                    }
                    System.out.println("Количество ставок клиента \"" + cl.getFullName() + "\": " + k + " шт.");
                }
            }
            System.out.println("\nВведите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) admin.showAbilities();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    void getClientList() {
        System.out.println("Вывести список пользователей, отсортированный по:\n 1) ФИО\n 2) Дате рождения");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet;
            String k;
            do {
                k = in.nextLine();
                switch (k) {
                    case "1":
                        resultSet = statement.executeQuery("select surname, name, patronymic from Users order by (surname, name, patronymic)");
                        if (resultSet.isBeforeFirst()) {
                            System.out.println("\nСписок пользователей:");
                            int i = 1;
                            while (resultSet.next()) {
                                System.out.println(" " + i + ") " + resultSet.getString(1) + " " + resultSet.getString(2).charAt(0) + "." +
                                        resultSet.getString(3).charAt(0) + ".");
                                i++;
                            }
                        } else System.out.println("Пока не добавлено ни одного пользователя");
                        break;
                    case "2":
                        TreeMap<Date, String> clientList = new TreeMap<>();
                        Date dateOfBirth;
                        String name;
                        resultSet = statement.executeQuery("select * from Users");
                        if (resultSet.isBeforeFirst()) {
                            while (resultSet.next()) {
                                try {
                                    dateOfBirth = new SimpleDateFormat("d.MM.yyyy").parse(resultSet.getString(5));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    dateOfBirth = new Date();
                                }
                                name = resultSet.getString(2) + " " + resultSet.getString(3).charAt(0) + "." +
                                        resultSet.getString(4).charAt(0) + ".";
                                clientList.put(dateOfBirth, name);
                            }
                            int j = 1;
                            System.out.println("\nСписок пользователей:");
                            for (Map.Entry<Date, String> item : clientList.entrySet()) {
                                System.out.println(" " + j + ") " + item.getValue());
                                j++;
                            }
                        } else System.out.println("Пока не добавлено ни одного пользователя");
                        break;
                    default:
                        System.out.println("Введите число 1 или 2!");
                        break;
                }
            } while (!k.equals("1") && !k.equals("2"));
            System.out.println("\nВведите \"0\", чтобы выйти");
            if (in.nextLine().equals("0")) admin.showAbilities();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    Client clientLogIn() {
        Connection connection = null;
        Client cc = null;
        try {
            connection = DriverManager.getConnection(URL, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            String login;
            String password;
            System.out.print("Введите логин: ");
            login = in.nextLine();
            System.out.print("Введите пароль: ");
            password = in.nextLine();
            ResultSet resultSet = statement.executeQuery("select * from Users where login=\'" + login + "\' and password=\'" + password + "\'");
            if (!resultSet.next()) {
                System.out.println("Неверный логин или пароль!\n");
            } else {
                cc = new Client(resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
                        resultSet.getString(5), resultSet.getString(1));
                cc.setLogin(login);
                cc.setPassword(password);
                System.out.println("Вы вошли как \"" + cc.getFullName() + "\"");
            }
            return cc;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
