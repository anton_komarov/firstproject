package com.komarov;

import java.util.Scanner;

public abstract class UserDAO {
    abstract boolean showAllClients();
    abstract boolean listIsEmpty();
    abstract Client addClient();
    abstract void showLots(User user);
    abstract Client selectClient(int c);

    abstract void newLot();
    abstract void showMyLotsAndStavki();

    abstract void getAllStavki();
    abstract void getUsersLots();
    abstract void getUsersStavki();
    abstract void getClientList();

    abstract Client clientLogIn();
    boolean adminLogIn() {
        boolean res;
        Scanner in = new Scanner(System.in);
        String login;
        String password;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        if (login.equals("admin") && password.equals("admin")) {
            res = true;
        } else {
            System.out.println("Неверный логин или пароль!\n");
            res = false;
        }
        return res;
    }
}
