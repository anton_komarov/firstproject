package com.komarov;

import java.util.*;

public class Admin extends User {

    public Admin() {
        switch (Main.getUserDaoConstant()) {
            case 1:
                userDAO = new CollectionsUserDAO(this);
                break;
            case 2:
                userDAO = new FilesUserDAO(this);
                break;
            case 3:
                userDAO = new DatabaseUserDAO(this);
                break;
            case 4:
                userDAO = new JPAUserDAO(this);
                break;
        }
    }

    @Override
    public void showAbilities() {
        System.out.print("\nВыберите из списка, что вы хотите сделать:\n 1) Посмотреть список всех лотов\n 2) Посмотреть список всех ставок\n");
        System.out.print(" 3) Посмотреть количество лотов пользователя\n 4) Посмотреть количество ставок пользователя\n");
        System.out.println(" 5) Добавить новый лот\n 6) Посмотреть список пользователей\n 0) Выйти");
        Scanner in = new Scanner(System.in);
        String k;
        do {
            k = in.nextLine();
            switch (k) {
                case "1":
                    userDAO.showLots(this);
                    break;
                case "2":
                    userDAO.getAllStavki();
                    break;
                case "3":
                    userDAO.getUsersLots();
                    break;
                case "4":
                    userDAO.getUsersStavki();
                    break;
                case "5":
                    userDAO.newLot();
                    break;
                case "6":
                    userDAO.getClientList();
                    break;
                case "0":
                    Main.login();
                    break;
                default:
                    System.out.println("Введите число от 0 до 6");
            }
        } while (!k.equals("1") && !k.equals("2") && !k.equals("3") && !k.equals("4") && !k.equals("5") && !k.equals("6") && !k.equals("0"));
    }
}
