package com.komarov;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JPAUserDAO extends UserDAO {

    private Client client;
    private Admin admin;
    private Scanner in;
    private SessionFactory sessionFactory;

    public JPAUserDAO() {
        in = new Scanner(System.in);
        sessionFactory = SessionFactoryUtil.getSessionFactory();
    }

    public JPAUserDAO(User user) {
        in = new Scanner(System.in);
        sessionFactory = SessionFactoryUtil.getSessionFactory();
        if (user instanceof Client) {
            this.client = (Client) user;
        }
        if (user instanceof Admin) {
            this.admin = (Admin) user;
        }
    }

    @Override
    boolean showAllClients() {
        boolean res;
        List<Client> list = getObjList("from Client");
        if (list.isEmpty()) {
            System.out.println("Пока не добавлено ни одного пользователя");
            res = false;
        } else {
            System.out.println("Выберите пользователя:");
            int i = 1;
            for (Client c : list) {
                System.out.println(" " + i + ") " + c.getFullName());
                i++;
            }
            res = true;
        }
        return res;
    }

    @Override
    boolean listIsEmpty() {
        return getObjList("from Client").isEmpty();
    }

    @Override
    Client addClient() {
        String name;
        String surname;
        String patronymic;
        String date;
        String login;
        String password;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        System.out.println("\nВведите данные нового пользователя");
        System.out.print(" Фамилия: ");
        surname = in.nextLine();
        System.out.print(" Имя: ");
        name = in.nextLine();
        System.out.print(" Отчество: ");
        patronymic = in.nextLine();
        System.out.print(" Дата рождения (в формате дд.мм.гггг): ");
        date = in.nextLine();
        client = new Client(surname, name, patronymic, date, String.valueOf(System.nanoTime()));
        client.setLogin(login);
        client.setPassword(password);
        try {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.save(client);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println("Такой логин уже существует, выберите другой!");
            addClient();
        }
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(client);
        transaction.commit();
        session.close();
        System.out.println("\nПользователь \""+surname+" "+name+" "+patronymic+"\" добавлен!");
        return client;
    }

    @Override
    void showLots(User user) {
        System.out.println("Список всех лотов:");
        Session session = sessionFactory.openSession();
        List<String> list = (List<String>) session.createQuery("select lts.name from Lots as lts group by lts.name").list();
        session.close();
        if (list.isEmpty()) {
            System.out.println("Пока не добавлено ни одного лота");
        } else {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(" " + (i + 1) + ") " + list.get(i));
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) user.showAbilities();
    }

    @Override
    Client selectClient(int c) {
        Session session = sessionFactory.openSession();
        List<Client> list = (List<Client>) session.createQuery("from Client").setMaxResults(c).list();
        session.close();
        return new Client(list.get(c - 1).getSurname(), list.get(c - 1).getName(), list.get(c - 1).getPatronymic(), list.get(c - 1).getStringDate(), list.get(c - 1).getId());
    }

    @Override
    void newLot() {
        System.out.println("Добавление нового лота");
        System.out.print(" Введите название лота: ");
        String newLotName = in.nextLine();
        Lots l = new Lots(newLotName);
        Session session = sessionFactory.openSession();
        List<Lots> list = (List<Lots>) session.createQuery("from Lots as lts where lts.name = \'" + newLotName + "\'").list();
        session.close();
        if (list.isEmpty()) {
            if (client != null) {
                l.setUser_id(client.getId());
                System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
                if (in.nextLine().equals("1")) {
                    System.out.print("Введите ставку: ");
                    String stavka = in.nextLine();
                    l.setStavka(Double.parseDouble(stavka));
                    saveObj(l);
                    System.out.println("Лот \"" + newLotName + "\" добавлен!");
                    System.out.println("\nВведите \"0\", чтобы выйти");
                    if (in.nextLine().equals("0")) client.showAbilities();
                } else {
                    saveObj(l);
                    System.out.println("Лот \"" + newLotName + "\" добавлен!");
                    client.showAbilities();
                }
            } else {
                l.setUser_id("admin");
                saveObj(l);
                System.out.println("Лот \"" + newLotName + "\" добавлен!");
                admin.showAbilities();
            }
        } else {
            System.out.println("\nТакой лот уже существует!");
            if (client != null) {
                l.setUser_id(client.getId());
                System.out.print("Сделать ставку по этому лоту?\n 1) Да\n 0) Нет\n");
                if (in.nextLine().equals("1")) {
                    System.out.print("Введите ставку: ");
                    String stavka = in.nextLine();
                    l.setStavka(Double.parseDouble(stavka));
                    saveObj(l);
                    System.out.println("Ставка к лоту \"" + newLotName + "\" добавлена!");
                    System.out.println("\nВведите \"0\", чтобы выйти");
                    if (in.nextLine().equals("0")) client.showAbilities();
                } else {
                    saveObj(l);
                    client.showAbilities();
                }
            } else {
                admin.showAbilities();
            }
        }

    }

    @Override
    void showMyLotsAndStavki() {
        System.out.println("Просмотр списка своих ставок по лоту:");
        List<String> lotsName = getObjList("select lts.name from Lots as lts where lts.user_id = \'" + client.getId() + "\' group by lts.name");
        if (lotsName.isEmpty()) {
            System.out.println("У вас нет лотов!");
        } else {
            for (int i = 0; i < lotsName.size(); i++) {
                System.out.println(" " + (i + 1) + ") " + lotsName.get(i));
                List<Double> stavki = getObjList("select lts.stavka from Lots as lts where lts.name = \'" + lotsName.get(i) + "\' " +
                        "and lts.user_id = \'" + client.getId() + "\' and lts.stavka != \'-1\'");
                if (!stavki.isEmpty()) {
                    for (int j = 0; j < stavki.size(); j++) {
                        System.out.println("    " + (j + 1) + ") " + stavki.get(j));
                    }
                } else System.out.println("    (нет ставок)");
                System.out.println();
            }
        }
        System.out.println("Введите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) client.showAbilities();
    }

    @Override
    void getAllStavki() {
        System.out.println("Просмотр списка всех ставок");
        List<String> lotsName = getObjList("select lts.name from Lots as lts group by lts.name");
        if (lotsName.isEmpty()) {
            System.out.println("Пока не добавлено ни одного лота");
        } else {
            for (int i = 0; i < lotsName.size(); i++) {
                System.out.println(" " + (i + 1) + ") " + lotsName.get(i));
                List<Double> stavki = getObjList("select lts.stavka from Lots as lts where lts.name = \'" + lotsName.get(i) + "\' and lts.stavka != \'-1\'");
                if (!stavki.isEmpty()) {
                    for (int j = 0; j < stavki.size(); j++) {
                        System.out.println("    " + (j + 1) + ") " + stavki.get(j));
                    }
                } else System.out.println("    (нет ставок)");
                System.out.println();
            }
        }
        System.out.println("Введите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    void getUsersLots() {
        System.out.println("Просмотр количества лотов пользователя");
        if (showAllClients()) {
            Client cl = selectClient(Integer.parseInt(in.nextLine()));
            System.out.println("Количество лотов клиента \"" + cl.getFullName() + "\": " +
                    getObjList("select lts.name from Lots as lts where lts.user_id = \'" + cl.getId() + "\' group by lts.name").size() + " шт.");
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    void getUsersStavki() {
        System.out.println("Просмотр количества ставок пользователя");
        if (showAllClients()) {
            Client cl = selectClient(Integer.parseInt(in.nextLine()));
            List<Lots> list = getObjList("from Lots as lts where (lts.stavka != \'-1\' and lts.user_id = \'" + cl.getId()
                    + "\')");
            if (list.isEmpty()) {
                System.out.println("У пользователя \"" + cl.getFullName() + "\" нет лотов!");
            } else {
                System.out.println("Количество ставок клиента \"" + cl.getFullName() + "\": " + list.size() + " шт.");
            }
        }
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    void getClientList() {
        System.out.println("Вывести список пользователей, отсортированный по:\n 1) ФИО\n 2) Дате рождения");
        String k;
        do {
            k = in.nextLine();
            switch (k) {
                case "1":
                    List<Client> list1 = getObjList("from Client as cl order by cl.surname, cl.name, cl.patronymic");
                    if (!list1.isEmpty()) {
                        System.out.println("\nСписок пользователей:");
                        int i = 1;
                      for (Client c : list1) {
                          System.out.println(" " + i + ") " + c.getFullName());
                          i++;
                      }
                    } else System.out.println("Пока не добавлено ни одного пользователя");
                    break;
                case "2":
                    List<Client> list2 = getObjList("from Client as cl order by cl.dateOfBirth");
                    if (!list2.isEmpty()) {
                        System.out.println("\nСписок пользователей:");
                        int i = 1;
                        for (Client c : list2) {
                            System.out.println(" " + i + ") " + c.getFullName());
                            i++;
                        }
                    } else System.out.println("Пока не добавлено ни одного пользователя");
                    break;
                default:
                    System.out.println("Введите число 1 или 2!");
                    break;
            }
        } while (!k.equals("1") && !k.equals("2"));
        System.out.println("\nВведите \"0\", чтобы выйти");
        if (in.nextLine().equals("0")) admin.showAbilities();
    }

    @Override
    Client clientLogIn() {
        String login;
        String password;
        System.out.print("Введите логин: ");
        login = in.nextLine();
        System.out.print("Введите пароль: ");
        password = in.nextLine();
        Client cc = null;
        Session session = sessionFactory.openSession();
        Client c = session.get(Client.class, login);
        session.close();
        if (c != null && c.getPassword().equals(password)) {
            cc = new Client(c.getSurname(), c.getName(), c.getPatronymic(), c.getStringDate(), c.getId());
            cc.setLogin(c.getLogin());
            cc.setPassword(c.getPassword());
            System.out.println("Вы вошли как \"" + cc.getFullName() + "\"");
        } else {
            System.out.println("Неверный логин или пароль!\n");
        }
        return cc;
    }

    private void saveObj(Object object) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(object);
        transaction.commit();
        session.close();
    }

    private List getObjList(String query) {
        Session session = sessionFactory.openSession();
        List<Object> list = (List<Object>) session.createQuery(query).list();
        session.close();
        return list;
    }
}
