package com.komarov;

import java.util.Scanner;

public class Guest extends User {

    public Guest() {
        switch (Main.getUserDaoConstant()) {
            case 1:
                userDAO = new CollectionsUserDAO(this);
                break;
            case 2:
                userDAO = new FilesUserDAO(this);
                break;
            case 3:
                userDAO = new DatabaseUserDAO(this);
                break;
            case 4:
                userDAO = new JPAUserDAO(this);
                break;
        }
    }

    @Override
    public void showAbilities() {
        System.out.println("\nВыберите из списка, что вы хотите сделать:\n 1) Посмотреть список всех лотов\n 0) Выход");
        Scanner in = new Scanner(System.in);
        String k;
        do {
            k = in.nextLine();
            switch (k) {
                case "1":
                    userDAO.showLots(this);
                    break;
                case "0":
                    Main.login();
                    break;
                default:
                    System.out.println("Введите число 0 или 1");
            }
        } while (!k.equals("1") && !k.equals("0"));
    }
}
