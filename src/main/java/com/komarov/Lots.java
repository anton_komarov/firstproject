package com.komarov;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@Table(name = "lots_hibernate")
public class Lots {
    @Id
    @GeneratedValue
    private int id;

    private String name;
    private String user_id;
    private double stavka = -1;

    @Transient
    private ArrayList<Double> stavki;

    public Lots() {
    }

    public Lots(String name) {
        this.name = name;
        stavki = new ArrayList<>();
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public double getStavka() {
        return stavka;
    }

    public void setStavka(double stavka) {
        this.stavka = stavka;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Double> getStavki() {
        return stavki;
    }
}
