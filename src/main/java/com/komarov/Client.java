package com.komarov;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

@Entity
@Table(name = "users_hibernate")
public class Client extends User {

private String id;
private String name;
private String surname;
private String patronymic;

@Id
private String login;

private String password;
private Date dateOfBirth;

@Transient
private ArrayList<Lots> myLots;

@Transient
private int[][] myStavki;

    public Client() {
    }

    public Client(String surname, String name, String patronymic, String dateString, String id) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.id = id;
        DateFormat format = new SimpleDateFormat("d.MM.yyyy");
        try {
            dateOfBirth = format.parse(dateString);
        } catch (ParseException e) {
            dateOfBirth = new Date();
            e.printStackTrace();
        }

        switch (Main.getUserDaoConstant()) {
            case 1:
                myLots = new ArrayList<>();
                myStavki = new int[20][10];   //[мои лоты][мои ставки по лоту]
                userDAO = new CollectionsUserDAO(this);
                break;
            case 2:
                userDAO = new FilesUserDAO(this);
                break;
            case 3:
                userDAO = new DatabaseUserDAO(this);
                break;
            case 4:
                userDAO = new JPAUserDAO(this);
                break;
        }
    }

    @Override
    public void showAbilities() {
        System.out.print("\nВыберите из списка, что вы хотите сделать:\n 1) Посмотреть список всех лотов\n 2) Посмотреть список своих лотов и ставок по ним\n");
        System.out.println(" 3) Добавить новый лот\n 0) Выйти");
        Scanner in = new Scanner(System.in);
        String k;
        do {
            k = in.nextLine();
            switch (k) {
                case "1":
                    userDAO.showLots(this);
                    break;
                case "2":
                    userDAO.showMyLotsAndStavki();
                    break;
                case "3":
                    userDAO.newLot();
                    break;
                case "0":
                    Main.login();
                    break;
                default:
                    System.out.println("Введите число от 0 до 3");
            }
        } while (!k.equals("1") && !k.equals("2") && !k.equals("3") && !k.equals("0"));
    }

    public Double getStavka(int Lot, int StavkaNumber) {
        return myLots.get(Lot).getStavki().get(myStavki[Lot][StavkaNumber] - 1);
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return surname + " " + name.charAt(0) + "." + patronymic.charAt(0) + ".";
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getStringDate() {
        String newDate = new SimpleDateFormat("dd.MM.yyyy").format(dateOfBirth);
        return newDate;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Lots> getMyLots() {
        return myLots;
    }

    public int[][] getMyStavki() {
        return myStavki;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
